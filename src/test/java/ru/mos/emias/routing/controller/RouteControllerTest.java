package ru.mos.emias.routing.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import ru.mos.emias.routing.AbstractApplicationTest;
import ru.mos.emias.routing.JsonConverter;
import ru.mos.emias.routing.model.dto.PropertyDto;
import ru.mos.emias.routing.model.dto.RequestUserPropertiesDto;
import ru.mos.emias.routing.model.dto.ResponseDto;
import ru.mos.emias.routing.model.dto.UserDataTargetDto;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

class RouteControllerTest extends AbstractApplicationTest {

    @ParameterizedTest
    @CsvSource({
        "getURL_v2_request.json, getURL_v2_response.json",
        "getURL_v3_request.json, getURL_v3_response.json",
        "getURL_v4_request.json, getURL_v4_response.json",
        "getURL_v5_request.json, getURL_v5_response.json",
        "getURL_v6_request.json, getURL_v6_response.json",
        "getURL_v7_request.json, getURL_v7_response.json",
        "getURL_v8_request.json, getURL_v8_response.json",
        "getURL_v9_request.json, getURL_v9_response.json",
        "getURL_v10_request.json, getURL_v10_response.json",
        "getURL_v11_request.json, getURL_v11_response.json",
        "getURL_v12_request.json, getURL_v12_response.json"
    })
    void getURL(String requestFilename, String responseFilename) throws IOException {
        RequestUserPropertiesDto request = JsonConverter.readJsonFromFile(getClass(), requestFilename,
            new TypeReference<>() {});
        ResponseEntity<ResponseDto<UserDataTargetDto>> actualResponse = makePost(new ParameterizedTypeReference<>() {},
            new HttpEntity<>(request));

        ResponseDto<UserDataTargetDto> expectedResponse = JsonConverter.readJsonFromFile(getClass(), responseFilename,
            new TypeReference<>() {});
        Assertions.assertNotNull(actualResponse.getBody());
        if ("getURL_v11_request.json".equals(requestFilename) || "getURL_v12_request.json".equals(requestFilename)) {
            Assertions.assertEquals(expectedResponse.getData().getProperties(),
                actualResponse.getBody().getData().getProperties());
        } else {
            assertSetEquals(expectedResponse.getData().getProperties(),
                actualResponse.getBody().getData().getProperties());
        }
    }

    @ParameterizedTest
    @CsvSource({
        "getError_v1_request.json, getError_v1_response.json",
        "getError_v2_request.json, getError_v2_response.json"
    })
    void getError(String requestFilename, String responseFilename) throws IOException {
        RequestUserPropertiesDto request = JsonConverter.readJsonFromFile(getClass(), requestFilename,
            new TypeReference<>() {});
        ResponseEntity<ResponseDto<UserDataTargetDto>> actualResponse = makePost(new ParameterizedTypeReference<>() {},
            new HttpEntity<>(request));

        ResponseDto<UserDataTargetDto> expectedResponse = JsonConverter.readJsonFromFile(getClass(), responseFilename,
            new TypeReference<>() {});
        Assertions.assertNotNull(actualResponse.getBody());
        Assertions.assertEquals(expectedResponse.getData(), actualResponse.getBody().getData());
    }

    private static void assertSetEquals(List<PropertyDto> inOld, List<PropertyDto> inNew) {
        Assertions.assertEquals(inOld.size(), inNew.size());
        inOld.sort(Comparator.comparing(PropertyDto::getName));
        inNew.sort(Comparator.comparing(PropertyDto::getName));
        Assertions.assertEquals(inOld, inNew);
    }
}
