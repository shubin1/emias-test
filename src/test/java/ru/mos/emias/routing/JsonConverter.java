package ru.mos.emias.routing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Slf4j
@UtilityClass
public class JsonConverter {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * Преообразует строку json к объекту заданного типа.
     *
     * @param inJson строка json
     * @param tClass тип возвращаемого объекта
     * @param <T>    тип возвращаемого объекта
     * @return объект типа <T> созданный на основе считанного json
     * @throws JsonProcessingException если во время преобразования что-то пошло не так
     */
    public static <T> T fromJson(String inJson, TypeReference<T> tClass) throws JsonProcessingException {
        try {
            return MAPPER.readValue(inJson, tClass);
        } catch (JsonProcessingException e) {
            log.error("Json parse error", e);
            throw e;
        }
    }

    /**
     * Считывает строку json из файла и приобразует к объекту заданного типа.
     *
     * @param tClass   класс, где вызывается метод getResourceAsStream,
     *                 для облегчения поиска файла, так как файл лежит в той же папке
     * @param filename имя файла, откуда считыватеся
     * @param inClass  тип возвращаемого объекта
     * @param <T>      тип возвращаемого объекта
     * @return объект типа <T> созданный на основе считанного json
     * @throws IOException в try блоке была поймана ошибка
     */
    public static <T> T readJsonFromFile(Class<?> tClass, String filename, TypeReference<T> inClass) throws IOException {
        try {
            String inJson = getJsonStringFromFile(tClass, filename);
            return fromJson(inJson, inClass);
        } catch (IOException e) {
            log.error("Json parse error", e);
            throw e;
        }
    }

    /**
     * Считывает строку json из файла
     *
     * @param tClass   класс, где вызывается метод getResourceAsStream,
     *                 для облегчения поиска файла, так как файл лежит в той же папке
     * @param filename названия файла
     * @return строку, считанную из файла
     * @throws IOException если что-то пошло не так с преобразованием в строку
     */
    public static String getJsonStringFromFile(Class<?> tClass, String filename) throws IOException {
        InputStream resourceStream = tClass.getResourceAsStream(filename);
        return IOUtils.toString(resourceStream, StandardCharsets.UTF_8.name());
    }
}
