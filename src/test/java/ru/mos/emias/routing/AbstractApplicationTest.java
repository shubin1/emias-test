package ru.mos.emias.routing;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
@AutoConfigureEmbeddedDatabase
public abstract class AbstractApplicationTest {

    @Autowired
    protected TestRestTemplate testRestTemplate;

    protected <T> ResponseEntity<T> makePost(
        ParameterizedTypeReference<T> typeReference, HttpEntity httpEntity
    ) {
        ResponseEntity<T> exchange = testRestTemplate.exchange(
            "/getUrl",
            HttpMethod.POST,
            httpEntity,
            typeReference);
        return exchange;
    }
}
