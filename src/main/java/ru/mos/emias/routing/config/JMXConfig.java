package ru.mos.emias.routing.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import ru.mos.emias.routing.jmx.RulesBean;
import ru.mos.emias.routing.repository.LinkElementRepository;
import ru.mos.emias.routing.repository.PropertyRepository;
import ru.mos.emias.routing.repository.RuleRepository;

@Configuration
@EnableMBeanExport
public class JMXConfig {

    @Bean
    public RulesBean rulesBean(
            ObjectMapper objectMapper,
            RuleRepository ruleRepository,
            PropertyRepository propertyRepository,
            LinkElementRepository linkElementRepository
    ) {
        return new RulesBean(objectMapper, ruleRepository, propertyRepository, linkElementRepository);
    }
}
