package ru.mos.emias.routing.exception;

public enum ErrorCode {
    INVALID_REQUEST,
    NOT_FOUND
}
