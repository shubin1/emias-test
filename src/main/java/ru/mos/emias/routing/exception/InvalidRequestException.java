package ru.mos.emias.routing.exception;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class InvalidRequestException extends ApplicationException {

    private static final long serialVersionUID = 4864882415726386571L;

    @Getter
    private final ErrorCode code;
    @Getter
    private final List<Object> params = new ArrayList<>();

    public InvalidRequestException(ErrorCode code, String message, Object id) {
        super(message);
        this.code = code;
        params.add(id);
    }

    public InvalidRequestException(ErrorCode code, String message) {
        super(message);
        this.code = code;
    }
}
