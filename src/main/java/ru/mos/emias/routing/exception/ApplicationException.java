package ru.mos.emias.routing.exception;

/**
 * Общее класс для всех исключений приложения.
 */
@SuppressWarnings({"AbstractClassWithoutAbstractMethods", "AbstractClassExtendsConcreteClass"})
public abstract class ApplicationException extends RuntimeException {

    private static final long serialVersionUID = 3153154364516961014L;

    protected ApplicationException() {
    }

    protected ApplicationException(String message) {
        super(message);
    }

    protected ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    protected ApplicationException(Throwable cause) {
        super(cause);
    }

    protected ApplicationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
