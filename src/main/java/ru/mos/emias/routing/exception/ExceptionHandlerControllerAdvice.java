package ru.mos.emias.routing.exception;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.mos.emias.routing.model.dto.ErrorDto;
import ru.mos.emias.routing.model.dto.ResponseDto;

import javax.servlet.http.HttpServletRequest;

import java.util.Collections;
import java.util.Optional;

@RestControllerAdvice
@RequiredArgsConstructor
public class ExceptionHandlerControllerAdvice {

    private final MessageSource messageSource;

    /**
     * Отлавливает все CommonException, собиратет ResponseDto без данных, со списком errorDto,
     * предоставляющим информацию о перехваченных исключениях
     *
     * @param request   заспрос, который вызвал исключение
     * @param exception пойманное исключение
     * @return ResponseDto, состоящий из списка пойманных ошибок
     */
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(InvalidRequestException.class)
    public ResponseDto<Void> handleResourceNotFound(final HttpServletRequest request, final InvalidRequestException exception) {
        return Optional.of(messageSource.getMessage(exception.getMessage(), exception.getParams().toArray(), request.getLocale()))
            .map(message -> new ErrorDto(message, exception.getCode()))
            .map(Collections::singletonList)
            .map(ResponseDto::failure)
            .orElse(null);
    }
}
