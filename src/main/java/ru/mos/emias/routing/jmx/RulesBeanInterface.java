package ru.mos.emias.routing.jmx;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.util.List;

public interface RulesBeanInterface {

    List<RuleContainer> getRules();

    String getJsonRule(String name) throws JsonProcessingException;

    List<ParameterContainer> getParameters();

    String getJsonParameter(String name) throws JsonProcessingException;

    void addRule(String jsonRule) throws IOException;

    void deleteRule(String name);

    void addParameter(String jsonParameter) throws IOException;

    void deleteParameter(String name);
}
