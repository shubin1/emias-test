package ru.mos.emias.routing.jmx;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import ru.mos.emias.routing.model.entity.LinkElement;
import ru.mos.emias.routing.model.entity.Property;
import ru.mos.emias.routing.model.entity.Rule;
import ru.mos.emias.routing.repository.LinkElementRepository;
import ru.mos.emias.routing.repository.PropertyRepository;
import ru.mos.emias.routing.repository.RuleRepository;

import javax.management.StandardMBean;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@ManagedResource(
    objectName = "emias-bean:name=RulesBean",
    description = "Rules operations bean"
)
public class RulesBean extends StandardMBean implements RulesBeanInterface {

    private final ObjectMapper objectMapper;
    private final RuleRepository ruleRepository;
    private final PropertyRepository propertyRepository;
    private final LinkElementRepository linkElementRepository;

    public RulesBean(
        ObjectMapper objectMapper,
        RuleRepository ruleRepository,
        PropertyRepository propertyRepository,
        LinkElementRepository linkElementRepository
    ) {
        super(RulesBeanInterface.class, true);
        this.objectMapper = objectMapper;
        this.ruleRepository = ruleRepository;
        this.propertyRepository = propertyRepository;
        this.linkElementRepository = linkElementRepository;
    }

    @Override
    @ManagedAttribute(description = "rules")
    public List<RuleContainer> getRules() {
        return ruleRepository.findAll().stream()
            .map(RuleContainer::new)
            .collect(Collectors.toList());
    }

    @Override
    public String getJsonRule(String name) throws JsonProcessingException {
        Rule rule = ruleRepository.findByName(name);
        return objectMapper.writeValueAsString(
            new RuleTransport(
                rule.getId(),
                rule.getName(),
                rule.getTarget(),
                rule.getProperties().stream()
                    .map(p -> new LinkElementTransport(p.getProperty().getName(), p.getPropertyValue(), p.isAnyApplied(), p.getOperation()))
                    .collect(Collectors.toList()))
        );
    }

    @Override
    public List<ParameterContainer> getParameters() {
        return propertyRepository.findAll().stream()
            .map(ParameterContainer::new)
            .collect(Collectors.toList());
    }

    @Override
    public String getJsonParameter(String name) throws JsonProcessingException {
        return objectMapper.writeValueAsString(propertyRepository.findByName(name));
    }

    @Override
    public void addRule(String jsonRule) throws IOException {
        RuleTransport ruleTransport = objectMapper.readValue(jsonRule, RuleTransport.class);
        Rule rule = new Rule(ruleTransport.getId(), ruleTransport.getName(), ruleTransport.getTarget(), Set.of());

        Rule persistedRule = ruleRepository.save(rule);

        Set<LinkElement> linkElements = ruleTransport.getProperties().stream()
            .map(pt -> new LinkElement(
                null,
                persistedRule,
                propertyRepository.findByName(pt.getPropertyName()),
                pt.getValues(),
                pt.isAnyApplied(),
                pt.getOperation())
            )
            .collect(Collectors.toSet());

        linkElementRepository.saveAll(linkElements);
    }

    @Override
    public void deleteRule(String name) {
        ruleRepository.deleteByName(name);
    }

    @Override
    public void addParameter(String jsonParameter) throws IOException {
        Property property = objectMapper.readValue(jsonParameter, Property.class);
        propertyRepository.save(property);
    }

    @Override
    public void deleteParameter(String name) {
        propertyRepository.deleteByName(name);
    }
}
