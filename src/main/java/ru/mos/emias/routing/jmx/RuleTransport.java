package ru.mos.emias.routing.jmx;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RuleTransport {

    private Long id;

    private String name;

    private String target;

    private List<LinkElementTransport> properties;
}
