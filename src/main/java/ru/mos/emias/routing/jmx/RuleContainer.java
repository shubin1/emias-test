package ru.mos.emias.routing.jmx;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.RequiredArgsConstructor;
import ru.mos.emias.routing.model.entity.OperationEnum;
import ru.mos.emias.routing.model.entity.Rule;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class RuleContainer {

    @JsonIgnore
    private final Rule rule;

    public String getName() {
        return rule.getName();
    }

    public List<PropertyContainer> getProperties() {
        return rule.getProperties().stream()
            .map(p -> new PropertyContainer(
                p.getProperty().getName(),
                (p.getOperation() == OperationEnum.EQUALS ? "= " : "<> ")
                + (p.isAnyApplied() ? "All" : String.join(",", p.getPropertyValue()))
            ))
            .collect(Collectors.toList());
    }

    public String getTarget() {
        return rule.getTarget();
    }
}
