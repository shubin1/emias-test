package ru.mos.emias.routing.jmx;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.mos.emias.routing.model.entity.OperationEnum;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LinkElementTransport {

    private String propertyName;

    private List<String> values;

    private boolean isAnyApplied;

    private OperationEnum operation;
}
