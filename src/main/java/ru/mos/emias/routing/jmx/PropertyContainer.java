package ru.mos.emias.routing.jmx;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class PropertyContainer {

    private final String name;

    private final String values;
}
