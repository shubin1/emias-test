package ru.mos.emias.routing.jmx;

import lombok.RequiredArgsConstructor;
import ru.mos.emias.routing.model.entity.Property;

@RequiredArgsConstructor
public class ParameterContainer {

    private final Property property;

    public String getName() {
        return property.getName();
    }

    public String getType() {
        return property.getType();
    }

    public String getDescription() {
        return property.getDescription();
    }

    public Integer getRank() {
        return property.getRank();
    }
}
