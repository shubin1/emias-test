package ru.mos.emias.routing.controller;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.mos.emias.routing.model.dto.PropertyDto;
import ru.mos.emias.routing.model.dto.RequestUserPropertiesDto;
import ru.mos.emias.routing.model.dto.ResponseDto;
import ru.mos.emias.routing.model.dto.UserDataTargetDto;
import ru.mos.emias.routing.model.entity.Rule;
import ru.mos.emias.routing.service.RoutingService;

import java.util.List;
import java.util.stream.Collectors;

@Api
@RestController
@RequiredArgsConstructor
public class RouteController {

    private final RoutingService routingService;

    @PostMapping("/getUrl")
    ResponseDto<UserDataTargetDto> getURL(@RequestBody RequestUserPropertiesDto inUserPropertiesDto) {
        List<PropertyDto> props = inUserPropertiesDto.getProperties();
        List<Pair<String, String>> propertyNamesWithValue = props.stream()
            .map(e -> Pair.of(e.getName(), e.getValue()))
            .collect(Collectors.toList());

        Rule matchingRule = routingService.findMatchingRule(propertyNamesWithValue);
        return ResponseDto.success(new UserDataTargetDto(matchingRule.getTarget(), props));
    }
}
