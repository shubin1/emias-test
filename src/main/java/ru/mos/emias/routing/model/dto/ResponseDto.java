package ru.mos.emias.routing.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class ResponseDto<T> {

    private final Status status;

    private final T data;

    private final List<ErrorDto> errorDto;

    public ResponseDto(
            @JsonProperty(value = "status") Status status,
            @JsonProperty(value = "data") T data,
            @JsonProperty(value = "errorDto") List<ErrorDto> errorDto
    ) {
        this.status = status;
        this.data = data;
        this.errorDto = errorDto;
    }

    public static ResponseDto<Void> failure(List<ErrorDto> errorDtoList) {
        return new ResponseDto<>(Status.ERROR, null, errorDtoList);
    }

    public static <T> ResponseDto<T> success(T data) {
        return new ResponseDto<>(Status.OK, data, null);
    }
}
