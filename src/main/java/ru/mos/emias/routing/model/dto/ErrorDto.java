package ru.mos.emias.routing.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import ru.mos.emias.routing.exception.ErrorCode;

@Value
public class ErrorDto {

    private final String message;

    private final ErrorCode code;

    @JsonCreator
    public ErrorDto(@JsonProperty(value = "message") String message, @JsonProperty(value = "code")  ErrorCode code) {
        this.message = message;
        this.code = code;
    }
}
