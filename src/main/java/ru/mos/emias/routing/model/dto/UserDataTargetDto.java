package ru.mos.emias.routing.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDataTargetDto {

    private String targetUrl;

    private List<PropertyDto> properties;
}
