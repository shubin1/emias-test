package ru.mos.emias.routing.model.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import java.util.List;

@Data
@EqualsAndHashCode(exclude = {"rule", "property"})
@Entity(name = "rule_property_link")
@TypeDefs({@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)})
@AllArgsConstructor
@NoArgsConstructor
public class LinkElement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Rule rule;

    @ManyToOne
    private Property property;

    @Type(type = "jsonb")
    private List<String> propertyValue;

    private boolean isAnyApplied;

    @Enumerated(EnumType.STRING)
    private OperationEnum operation;
}
