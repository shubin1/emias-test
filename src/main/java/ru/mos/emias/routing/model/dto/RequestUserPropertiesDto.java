package ru.mos.emias.routing.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestUserPropertiesDto {

    private ArrayList<PropertyDto> properties;
}
