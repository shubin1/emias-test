package ru.mos.emias.routing.model.dto;

/**
 * Статус ответа сервера.
 */
public enum Status {
    OK,
    ERROR
}
