package ru.mos.emias.routing.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import ru.mos.emias.routing.exception.ErrorCode;
import ru.mos.emias.routing.exception.InvalidRequestException;
import ru.mos.emias.routing.model.entity.LinkElement;
import ru.mos.emias.routing.model.entity.OperationEnum;
import ru.mos.emias.routing.model.entity.Property;
import ru.mos.emias.routing.model.entity.Rule;
import ru.mos.emias.routing.repository.PropertyRepository;
import ru.mos.emias.routing.repository.RuleRepository;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoutingService {

    private static final Collector<Pair<Property, String>, LinkedList<Pair<Property, String>>, LinkedList<Pair<Property, String>>>
        LINKED_LIST_COLLECTOR =
        Collector.of(
            LinkedList::new,
            LinkedList::add,
            (rules1, rules2) -> {
                rules1.addAll(rules2);
                return rules1;
            }
        );

    private final PropertyRepository propertyRepository;
    private final RuleRepository ruleRepository;

    public Rule findMatchingRule(List<Pair<String, String>> propertyNamesWithValues) {
        LinkedList<Pair<Property, String>> propertiesWithValue = propertyNamesWithValues.stream()
            .map(e -> Pair.of(propertyRepository.findByName(e.getFirst()), e.getSecond()))
            .sorted(Comparator.comparing(p -> p.getFirst().getRank()))
            .collect(LINKED_LIST_COLLECTOR);
        int initialPropsSize = propertiesWithValue.size();

        List<Rule> rules = filterRules(ruleRepository.findAll(), propertiesWithValue);
        if (rules.isEmpty()) {
            throw new InvalidRequestException(ErrorCode.NOT_FOUND, "error.notFound");
        }

        // Pushing rules with "all" to the back of the list
        rules.sort(Comparator.comparing(r -> r.getProperties().stream().filter(LinkElement::isAnyApplied).count()));

        Rule rule = rules.get(0);
        if (rule.getProperties().size() != initialPropsSize) {
            throw new InvalidRequestException(ErrorCode.INVALID_REQUEST, "error.numberOfArguments");
        }

        return rule;
    }

    private static List<Rule> filterRules(List<Rule> rules, LinkedList<Pair<Property, String>> propertiesWithValues) {
        List<Rule> ruleList = rules;
        while (true) {
            if (propertiesWithValues.isEmpty() || ruleList.isEmpty()) {
                return ruleList;
            }

            Pair<Property, String> prop = propertiesWithValues.removeFirst();

            ruleList = ruleList.stream()
                .filter(r -> r.getProperties().stream()
                    .filter(p -> p.getProperty().getName().equals(prop.getFirst().getName()))
                    .anyMatch(p -> p.isAnyApplied()
                                   || (p.getOperation() == OperationEnum.EQUALS) == p.getPropertyValue().contains(prop.getSecond()))
                )
                .collect(Collectors.toList());
        }
    }
}
