package ru.mos.emias.routing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmiasTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmiasTestApplication.class, args);
    }
}
