package ru.mos.emias.routing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mos.emias.routing.model.entity.LinkElement;

public interface LinkElementRepository extends JpaRepository<LinkElement, Integer> {
}
