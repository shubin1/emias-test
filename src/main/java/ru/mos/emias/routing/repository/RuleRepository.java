package ru.mos.emias.routing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mos.emias.routing.model.entity.Rule;

public interface RuleRepository extends JpaRepository<Rule, Long> {

    Rule findByName(String name);

    void deleteByName(String name);
}
