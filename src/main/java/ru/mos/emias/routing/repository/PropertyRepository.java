package ru.mos.emias.routing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mos.emias.routing.model.entity.Property;

public interface PropertyRepository extends JpaRepository<Property, Integer> {

    Property findByName(String name);

    void deleteByName(String name);
}
